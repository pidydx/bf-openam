#########################
# Create base container #
#########################
FROM ubuntu:22.04 as base
LABEL maintainer="pidydx"

# Set app user and group
ENV APP_USER=tomcat
ENV APP_GROUP=tomcat

# Set version pins
ENV VERSION=14.8.3

# Set base dependencies
ENV BASE_DEPS ca-certificates tomcat9 openjdk-17-jre-headless openjdk-17-jdk-headless

# Set build dependencies
ENV BUILD_DEPS wget unzip

# Create app user and group
RUN groupadd -g 999 ${APP_GROUP} \
 && useradd -r -M -N -u 999 ${APP_USER} -g ${APP_GROUP} -d /usr/local/openam -s /usr/sbin/nologin

# Update and install base dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_DEPS} \
 && rm -rf /var/lib/apt/lists/*

##########################
# Create build container #
##########################
FROM base AS builder

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
COPY build.sh /usr/src/build.sh
WORKDIR /usr/src
RUN ./build.sh


##########################
# Create final container #
##########################
FROM base

# Finalize install
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

RUN update-ca-certificates \
 && chown -R ${APP_USER}:${APP_GROUP} /usr/local/openam \
 && mkdir -p /var/lib/openam \
 && chown -R ${APP_USER}:${APP_GROUP} /var/lib/openam \
 && ln -s /etc/tomcat9 /usr/local/openam/conf


# Prepare container
ENV CATALINA_HOME="/usr/share/tomcat9"
ENV CATALINA_BASE="/usr/local/openam"
ENV CATALINA_TMPDIR="/tmp"
ENV JAVA_HOME="/usr/lib/jvm/java-17-openjdk-amd64"
ENV JAVA_OPTS="-Djava.awt.headless=true"

EXPOSE 8080/tcp 1812/udp
VOLUME ["/etc/openam", "/var/lib/openam"]
USER $APP_USER

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["openam"]
