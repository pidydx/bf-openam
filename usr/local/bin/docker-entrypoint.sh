#!/bin/bash

set -e

if [ "$1" = 'openam' ]; then
    exec /usr/share/tomcat9/bin/catalina.sh run
fi

exec "$@"
