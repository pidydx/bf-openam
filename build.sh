#!/bin/bash

set -e
wget -nv https://github.com/OpenIdentityPlatform/OpenAM/releases/download/${VERSION}/openam-${VERSION}.zip
unzip openam-${VERSION}.zip

unzip openam/SSOConfiguratorTools-${VERSION}.zip -x legal-notices/* README sampleconfiguration sampleupgrade -d /usr/local/share/openam

mkdir -m 770 -p /usr/local/openam/{lib,logs,work}
mkdir -m 770 -p /usr/local/openam/webapps/openam
cd /usr/local/openam/webapps/openam
jar -xvf /usr/src/openam/OpenAM-${VERSION}.war 
echo 'configuration.dir="/var/lib/openam"' >> WEB-INF/classes/bootstrap.properties
